var fs     = require('fs');
var gulp   = require('gulp');
var change = require('gulp-change');
var gutil  = require('gulp-util');
var slim   = require('gulp-slim');
var tap    = require('gulp-tap');
var path   = require('path');

var data = load_data( 'data.json' )

function load_data( filename ) {
  if( fs.existsSync( filename ) ) {
    var content = fs.readFileSync( filename, "utf8" );
    return content ? JSON.parse( content ) : {};
  }
  else return {}
}

function inject_code( content ) {
  return content
    // link_to, image_tag, form_tag, form_for, _url -> '#'
    // hash dot access
    .replace( /^/, "- Hash.class_eval { def method_missing( name, *_args, &_block ); self[name.to_s]; end }\n" )
    // link_to
    .replace( /^/, "- def link_to( *args ); '<a href=\"' + args[1] + '\">' + args[0] + '</a>'; end\n" )
    // button_to
    .replace( /^/, "- def button_to( *args ); '<button>' + args[0] + '</button>'; end\n" )
    // link_to replace
    .replace( /(=\s*[link_to|button_to])/g, "=$1" )
    // render replace
    .replace( /[=]+\s*render\s*\(*\s*['|"]([^'"]+)['|"][ ]*\)*[ ]*/g, "include _$1" );
}

gulp.task('default', function(){
  gulp.src("./src/slim/*.slim")
    .pipe(tap(function(file, t) {
      // filename = path.basename( file.path )
      filename = file.path.substr( __dirname.length + 1 );
      gutil.log( 'Slim: ' + filename + ' ...' );
      return gulp.src(file.path)
        .pipe(change(inject_code))
        .pipe(slim({
          pretty: true,
          data: data[filename],
          require: [ 'slim/include' ],
          options: [ "encoding='utf-8'", "include_dirs=['./src/slim']" ]
        }))
        .pipe(gulp.dest('./dist/html/'));
    }))
});
