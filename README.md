# Intro
- Process slim files with gulp
- Pass dummy data to slim files (using *data.json*)

# Setup
`npm install`

# Build
`gulp`

# Notes
- data.json: hash with local path to slim files in keys
